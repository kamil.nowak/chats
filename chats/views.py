from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.urls import reverse
from django.views import generic
from rest_framework import viewsets, serializers

from chats import models, forms


def index(request):
    return render(request, 'chat/index.html', {
        'chats': models.Chat.objects.all()
    })


class ChatListView(generic.ListView):
    model = models.Chat
    context_object_name = 'chats'
    template_name = 'chat/index.html'
    queryset = models.Chat.objects.all()

    def get_queryset(self):
        return models.Chat.objects.filter(users__id=self.request.user.id)


class ChatCreateView(generic.CreateView):
    model = models.Chat
    template_name = 'chat/chat_new.html'
    form_class = forms.ChatForm
    success_url = '/'

    def get_form_kwargs(self):
        data = super().get_form_kwargs()
        data['user'] = self.request.user
        return data


# Serializacja
# JSON {"key": "value"} -> Python object

# Deserializacja
# Python object -> JSON {"key": "value"}

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username', 'first_name']


# Serializers define the API representation.
class ChatSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True)

    class Meta:
        model = models.Chat
        fields = ['started', 'users']


# ViewSets define the view behavior.
class ChatViewSet(viewsets.ModelViewSet):
    queryset = models.Chat.objects.all()
    serializer_class = ChatSerializer

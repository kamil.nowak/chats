from django import forms
from django.contrib.auth import get_user_model

from chats import models


class ChatForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=get_user_model().objects.all())

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def clean_users(self):
        data = self.cleaned_data['users']
        if self.user not in data:
            raise forms.ValidationError('You have forgotten about yourself')
        return data

    class Meta:
        model = models.Chat
        fields = ('users',)


class MessageForm(forms.ModelForm):
    class Meta:
        model = models.Message
        fields = ('chat', 'user_from', 'message_body')


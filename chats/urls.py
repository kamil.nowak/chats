from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers

from . import views


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'chats', views.ChatViewSet)


urlpatterns = [
    url(r'api/', include(router.urls)),
    path('', views.ChatListView.as_view(), name='chats'),
    path('create', views.ChatCreateView.as_view(), name='chats-create'),
]

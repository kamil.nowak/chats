from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from chats import models


class ChatViewTest(TestCase):
    def setUp(self) -> None:
        # when
        self.url = reverse('chats')
        self.user_1 = get_user_model().objects.create_superuser(username='admin', password='admin')
        self.user_2 = get_user_model().objects.create(username='jan')
        self.user_3 = get_user_model().objects.create(username='krzysztof')
        chat_1 = models.Chat.objects.create()
        chat_1.users.set([self.user_1, self.user_2, self.user_3])

    def test_chat_view(self):
        # when
        self.client.login(username='admin', password='admin')
        response = self.client.get(self.url)

        # then
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/index.html')
        self.assertContains(response, 'Chat #1')

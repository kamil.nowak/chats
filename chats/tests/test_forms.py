from unittest import mock

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from chats import forms


class ChatFormTest(TestCase):
    def test_chat_form_valid(self):
        users = [
            get_user_model().objects.create(username='krzysztof'),
            get_user_model().objects.create(username='jan')
        ]
        form = forms.ChatForm(
            user=users[0],
            data={
                'users': users,
            }
        )
        self.assertTrue(form.is_valid())

    def test_chat_form_invalid_when_current_user_not_provided(self):
        admin = get_user_model().objects.create_superuser(username='admin', password='admin')
        users = [
            get_user_model().objects.create(username='krzysztof'),
            get_user_model().objects.create(username='jan')
        ]
        self.client.login(username='admin', password='admin')
        form = forms.ChatForm(
            user=admin,
            data={
                'users': users,
            }
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'users': ['You have forgotten about yourself']})

    def test_user_form_invalid_when_empty_array_provided(self):
        form = forms.ChatForm(data={'users': []})
        self.assertFalse(form.is_valid())
